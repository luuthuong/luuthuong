[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Fira+Code&color=C50F0F&center=true&vCenter=true&multiline=true&width=600&height=200&lines=Hello+%2C+I'm+Thuong+;Welcome+to+my+profile+And+have+a+nice+day+!)](https://git.io/typing-svg)


<div align="justify">

<a href="https://www.instagram.com/lthuong200200/">
<img src="https://img.shields.io/badge/Instagram-%23E4405F.svg?style=for-the-badge&logo=Instagram&logoColor=white">
</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="https://www.linkedin.com/in/lthuong200200/">
<img src="https://img.shields.io/badge/Linkedin-%231DA1F2.svg?style=for-the-badge&logo=Linkedin&logoColor=white">
</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="https://www.facebook.com/lthuong02">
<img src="https://img.shields.io/badge/facebook-2CA5E0?style=for-the-badge&logo=facebook&logoColor=white">
</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="https://gitlab.com/luuthuong">
<img src="https://img.shields.io/badge/gitlab-330F63?style=for-the-badge&logo=gitlab&logoColor=white">
</a>

</div>
<p></p>
<p align="justify">
I have two years of experience developing systems, libraries UI, interfaces, and technological solutions to
make the web a better place. I'm dedicated to writing and refactoring clean, reusable, and scalable code in
Angular, .NET Core and sometimes React to make products more performance applying good practices and
development standards and strong creative and analytical skills.
</p>

> Tools and technologies that I have worked with and am interested in ⚙️
<table>
  <tr>
    <td align="center" width="96">
        <img src="https://techstack-generator.vercel.app/csharp-icon.svg" alt="icon" width="65" height="65" />
      <br>C#
    </td>
     <td align="center" width="96">
        <img src="https://skillicons.dev/icons?i=dotnet" width="48" height="48" alt="ASP.NET Core" />
      <br>ASP.NET
    </td>
    <td align="center" width="96">
        <img src="https://techstack-generator.vercel.app/js-icon.svg" alt="icon" width="65" height="65" />
      <br>Javascript
    </td>
    <td align="center" width="96">
        <img src="https://techstack-generator.vercel.app/ts-icon.svg" alt="icon" width="65" height="65" />
      <br>Typescript
    </td>
    <td align="center" width="96">
        <img src="https://techstack-generator.vercel.app/github-icon.svg" width="65" height="65" alt="GitHub" />
      <br>Github
    </td>
          <td align="center" width="96">
        <img src="https://techstack-generator.vercel.app/restapi-icon.svg" width="65" height="65" alt="Rest API" />
      <br>Rest API
    </td>
          <td align="center" width="96">
        <img src="https://techstack-generator.vercel.app/docker-icon.svg" width="65" height="65" alt="Rest API" />
      <br>Docker
    </td>
  </tr>
  <tr>
    <td align="center" width="96">
        <img src="https://skillicons.dev/icons?i=git" width="48" height="48" alt="Git" />
      <br>Git
    </td>
    <td align="center"  width="96">
        <img src="https://skillicons.dev/icons?i=gitlab" width="48" height="48" alt="GitLab" />
      <br>GitLab
    </td>
    <td align="center"  width="96">
        <img src="https://skillicons.dev/icons?i=html" width="48" height="48" alt="HTML" />
      <br>HTML
    </td>
    <td align="center" width="96">
        <img src="https://techstack-generator.vercel.app/sass-icon.svg" width="48" height="48" alt="css" />
      <br>CSS/SCSS
    </td>
    <td align="center"  width="96">
        <img src="https://skillicons.dev/icons?i=bootstrap" width="48" height="48" alt="bootstrap" />
      <br>Bootstrap
    </td>
    <td align="center" width="96">
        <img src="https://skillicons.dev/icons?i=tailwind" width="48" height="48" alt="tailwind" />
      <br>Tailwind
    </td>
    <td align="center" width="96">
        <img src="https://techstack-generator.vercel.app/webpack-icon.svg" width="48" height="48" alt="tailwind" />
      <br>Webpack
    </td>
  </tr>
 <tr>
 </tr>
</table>

## Recent GitHub Activity ✅
<!--START_SECTION:waka-->

```txt
From: 23 June 2023 - To: 13 October 2023

Total Time: 453 hrs 20 mins

TypeScript         246 hrs 46 mins ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   54.43 %
HTML               41 hrs 14 mins  ⣿⣿⣤⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   09.10 %
C#                 40 hrs 13 mins  ⣿⣿⣄⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   08.87 %
JSON               23 hrs 42 mins  ⣿⣤⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   05.23 %
C++                20 hrs 15 mins  ⣿⣄⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   04.47 %
SCSS               16 hrs 51 mins  ⣿⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   03.72 %
JavaScript         15 hrs 30 mins  ⣷⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   03.42 %
Go                 11 hrs 1 min    ⣶⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   02.43 %
SQL                5 hrs 9 mins    ⣤⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   01.14 %
Assembly           5 hrs 3 mins    ⣤⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   01.11 %
TSConfig           4 hrs 40 mins   ⣤⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   01.03 %
Binary             4 hrs 27 mins   ⣄⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   00.98 %
Other              4 hrs 21 mins   ⣄⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   00.96 %
XML                2 hrs 25 mins   ⣄⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   00.53 %
Markdown           2 hrs 11 mins   ⣄⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀   00.48 %
```

<!--END_SECTION:waka-->
